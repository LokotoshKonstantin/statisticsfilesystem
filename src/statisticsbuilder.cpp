#include "statisticsbuilder.h"

StatisticsBuilder::StatisticsBuilder(QDir* current_dir)
{
    current_dir_ = current_dir;
}

void StatisticsBuilder::process(){
    auto result = calculateStatisticsCollection(current_dir_);

    QVariant data;
    data.setValue(result);
    emit returnCalculatedResult(data);
}

CharacteristicsOfDir StatisticsBuilder::calculateStatisticsCollection(QDir* current_dir){
    current_dir->setSorting(QDir::Type);
    QStringList files_in_current_dir = current_dir->entryList(QDir::Files);

    CharacteristicsOfDir current;
    auto t =  QList<Group>();

    if (!files_in_current_dir.empty()){
        auto map_of_files = QMap<QString, QList<QString>>();

        CharacteristicsOfGroup current_characteristics_of_group;
        for (const auto& item : files_in_current_dir){
            auto current_file_with_dir = current_dir->path() + "/" + item;
            auto current_suffix = QFileInfo(item).suffix().toLower();
            if (!map_of_files.contains(current_suffix)) {
                map_of_files.insert(current_suffix, QList<QString>{current_file_with_dir});
            }
            else {
                map_of_files[current_suffix].append(current_file_with_dir);
            }

            current_characteristics_of_group.count_of_files += 1;
            current_characteristics_of_group.total_size += QFileInfo(current_file_with_dir).size();
        }

        current_characteristics_of_group.average_size = current_characteristics_of_group.total_size / current_characteristics_of_group.count_of_files;
        Group all;
        all.group_type = "all";
        all.characteristics = current_characteristics_of_group;
        t.append(all);

        for (const auto item : map_of_files.toStdMap()){
            t.append(generateGroup(item.first, item.second));
        }
    }
    auto count_folder_in_dir = current_dir->entryList(QDir::AllDirs | QDir::NoDotAndDotDot).count();
    current.count_of_directories = count_folder_in_dir;
    current.list_of_groups = t;
    return current;
}

CharacteristicsOfGroup StatisticsBuilder::calculateStatisticForCurrentGroup(const QList<QString>& list_files_of_group){
    CharacteristicsOfGroup result;
    for (const auto& file : list_files_of_group){
        result.count_of_files += 1;
        result.total_size += QFileInfo(file).size();
    }
    result.average_size = (int)(result.total_size / result.count_of_files);
    return result;
}

Group StatisticsBuilder::generateGroup(const QString& name_of_group, const QList<QString>& list_of_files){
    Group result;
    result.group_type = name_of_group;
    result.characteristics = calculateStatisticForCurrentGroup(list_of_files);
    return result;
}
