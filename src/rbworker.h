#ifndef RBWORKER_H
#define RBWORKER_H

#include <QObject>
#include <QDir>

#include "statisticsbuilder.h"

/*!
    \brief Класс, отвечащий за создание обьектов StatisticsBuilder, и отслеживание выисления статистики.
*/
class RBWorker : public QObject
{
    Q_OBJECT

private:
    StatisticsBuilder* sb;
    QDir* current_dir;

public:
    RBWorker(QDir* current_dir_);
    ~RBWorker();

    /*!
    Qt Slot, который запускает процесс выисления статистики.
    */
    Q_SLOT void process();

    /*!
    Qt Slot, который перенаправляет CharacteristicsOfDir в mainwindow.
    */
    Q_SLOT void processingResult(const QVariant& data);

    /*!
    Qt Signal, который останавливает вычисления.
    */
    Q_SIGNAL void finished();

    /*!
    Qt Signal, который возращает CharacteristicsOfDir в mainwindow
    \param[in] data Сгенерированная CharacteristicsOfDir, хранящаяся в QVariant
    */
    Q_SIGNAL void returnCalculatedResult(const QVariant& data);
};

#endif // RBWORKER_H
