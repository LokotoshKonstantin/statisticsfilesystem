#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QGroupBox>
#include <QLineEdit>
#include <QDir>
#include <QPushButton>
#include <QFileDialog>
#include <QListWidget>
#include <QDebug>
#include <QLabel>
#include <QLineEdit>
#include <QTableWidget>
#include <QScrollArea>

#include "session.h"

/*!
    \brief Класс главного окна интерфейса программы.
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QDir* current_dir;
    QDir* root_dir;

    QGridLayout* main_grlayout;

    QGridLayout* gridLayout;

    QLineEdit* le_root_directory;
    QLineEdit* le_current_directory;

    QListWidget* lw_list_directories_in_current;

    QLineEdit* number_of_subdirectories_lnedit;

    Session* current_session;

    /*!
    Процедура, инициализирующая виджеты на часте главного окна, которая отвечает за работу с директориями.
    */
    void initDirPathOfWindow();

    /*!
    Процедура, инициализирующая виджеты на часте главного окна, которая отображает статистику в директории.
    */
    void initInfoPathOfWindow();

    /*!
    Процедура, которая обновляет информацию насчет директории, используя CharacteristicsOfDir.
    \param[in] current_statistics Сгенерированная CharacteristicsOfDir, содержащая статистику директории.
    */
    void updateInformationAboutDir(const CharacteristicsOfDir& current_statistics);

    /*!
    Функция, которая создает QGroupBox со всеми необходимыми виджетами для каждой группы.
    \param[in] current_group Сгенерированная Group
    \param[out] QGroupBox
    */
    QGroupBox* createGroupBoxStatistic(const Group& current_group);

    /*!
    Qt Slot, который открывает Диалоговое окно для выбора корневой директории.
    */
    Q_SLOT void chooseRootDirectory();

    /*!
    Qt Slot, который поднимается на одну директорию вверх, от текущей и обновляет текущую.
    */
    Q_SLOT void directoryUp();

    /*!
    Qt Slot, который обновляет список итемов на QListWidget.
    */
    Q_SLOT void updateListWidget();

    /*!
    Qt Slot, который возращает CharacteristicsOfDir в mainwindow
    \param[in] current_item Сгенерированная CharacteristicsOfDir, хранящаяся в QVariant
    */
    Q_SLOT void itemDoubleClicked(QListWidgetItem* current_item);

    /*!
    Qt Signal, который возращает CharacteristicsOfDir в mainwindow
    \param[in] data Сгенерированная CharacteristicsOfDir, хранящаяся в QVariant
    */
    Q_SLOT void processingResult(const QVariant& data);
};
#endif // MAINWINDOW_H
