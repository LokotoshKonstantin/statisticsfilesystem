#ifndef STATISTICSBUILDER_H
#define STATISTICSBUILDER_H

#include <QObject>
#include <QDir>
#include <QVariant>

struct CharacteristicsOfGroup{
    int count_of_files;
    int total_size;
    int average_size;
    CharacteristicsOfGroup(){
        count_of_files = 0;
        total_size = 0;
        average_size = 0;
    }
};

struct Group{
    QString group_type = "all";
    CharacteristicsOfGroup characteristics;
};

struct CharacteristicsOfDir{
    QList<Group> list_of_groups;
    int count_of_directories;
};

Q_DECLARE_METATYPE(CharacteristicsOfDir);


/*!
    \brief Класс, отвечащий за подсчет статистики для каждой директории.
    Данный класс имеет только одну простую цель: подситать статистику и отправить в класс интерфейса.
*/
class StatisticsBuilder : public QObject
{
    Q_OBJECT
public:
    StatisticsBuilder(QDir* current_dir);

    /*!
    Процедура для подсчета статистики для директории и вызова сигнала.
    */
    void process();

    /*!
    Qt Signal, который возращает CharacteristicsOfDir в mainwindow.
    \param[in] data Сгенерированная CharacteristicsOfDir, хранящаяся в QVariant
    */
    Q_SIGNAL void returnCalculatedResult(const QVariant& data);
private:
    QDir* current_dir_;

    /*!
    Генерирует CharacteristicsOfDir для определенной директории.
    \param[in] current_dir Директория, для которой генерируется CharacteristicsOfDir
    */
    CharacteristicsOfDir calculateStatisticsCollection(QDir* current_dir);

    /*!
    Генерирует CharacteristicsOfGroup для списка файлов определенной группы.
    \param[in] list_files_of_group Список названия файлов группы
    */
    CharacteristicsOfGroup calculateStatisticForCurrentGroup(const QList<QString>& list_files_of_group);

    /*!
    Генерирует Group для группы файлов.
    \param[in] name_of_group Название расширния группы.
    \param[in] list_of_files Список названия файлов группы
    */
    Group generateGroup(const QString& name_of_group, const QList<QString>& list_of_files);

};

#endif // STATISTICSBUILDER_H
