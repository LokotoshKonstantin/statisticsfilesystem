#ifndef SESSION_H
#define SESSION_H

#include <QObject>
#include <QDir>
#include <QThread>

#include "rbworker.h"

/*!
    \brief Класс, отвечащий за выделения потоков для подсчета статистики.
    Данный класс имеет только одну простую цель: создать поток, начать вычисления и по окончанию вычислений удалить поток.
*/
class Session : public QObject
{
    Q_OBJECT
public:
    Session(QObject *parent = nullptr);
    ~Session();

    /*!
    Создает поток, в котором считается статистика директории.
    \param[in] current_dir Директория, для которой генерируется статистика в отдельном потоке
    */
    void addThread(QDir* current_dir);

    /*!
    Останавливает работу потоков.
    */
    void stopThreads();

    /*!
    Qt Slot, который перенаправляет CharacteristicsOfDir в mainwindow.
    */
    Q_SLOT void processingResult(const QVariant& data);

    /*!
    Qt Signal, который останавливает работу потоков
    */
    Q_SIGNAL void stopAll();

    /*!
    Qt Signal, который возращает CharacteristicsOfDir в mainwindow.
    \param[in] data Сгенерированная CharacteristicsOfDir, хранящаяся в QVariant
    */
    Q_SIGNAL void returnCalculatedResult(const QVariant& data);
};

#endif // SESSION_H
