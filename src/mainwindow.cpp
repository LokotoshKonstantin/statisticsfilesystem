#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)

{
    qRegisterMetaType<CharacteristicsOfDir>();
    current_dir = new QDir(QDir::root());
    root_dir = new QDir(QDir::root());

    current_session = new Session();
    QObject::connect(current_session, &Session::returnCalculatedResult, this, &MainWindow::processingResult);

    auto w = new QWidget();
    main_grlayout = new QGridLayout(w);

    initDirPathOfWindow();
    initInfoPathOfWindow();

    setCentralWidget(w);
    setMinimumSize(400, 500);
    updateListWidget();
}

MainWindow::~MainWindow()
{
    delete current_session;
}

void MainWindow::chooseRootDirectory(){
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                root_dir->path(),
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);
    if (!dir.isEmpty()){
        current_dir->setPath(dir);
        root_dir->setPath(dir);
        le_root_directory->setText(current_dir->path());
        le_current_directory->setText(root_dir->path());
        updateListWidget();
    }
}

void MainWindow::directoryUp(){
    if (current_dir->path() != root_dir->path()){
        auto save_dir = current_dir->path();
        if (current_dir->cdUp()){
            le_current_directory->setText(current_dir->path());
            updateListWidget();
        }
    }
}

void MainWindow::updateListWidget(){
    lw_list_directories_in_current->clear();
    QStringList subdirs = current_dir->entryList(QDir::NoDot | QDir::NoDotDot | QDir::Dirs);
    if (!subdirs.empty()){
        for (const auto& item : subdirs){
            lw_list_directories_in_current->addItem(item);
        }
    }
    current_session->addThread(current_dir);
}

void MainWindow::itemDoubleClicked(QListWidgetItem* current_item){
    current_dir->setPath(current_dir->path() + "/" + current_item->text());
    le_current_directory->setText(current_dir->path());
    updateListWidget();
}

void MainWindow::processingResult(const QVariant& data){
    CharacteristicsOfDir ds = data.value<CharacteristicsOfDir>();
    updateInformationAboutDir(ds);
}

void MainWindow::initDirPathOfWindow(){
    QGroupBox* gb_directory= new QGroupBox("Work with Directory");
    gb_directory->setStyleSheet("QGroupBox { font-weight: bold; } ");

    auto gl_directory = new QGridLayout(gb_directory);
    main_grlayout->addWidget(gb_directory, 0, 0, 0, 6);

    QGroupBox* gb_root_directory= new QGroupBox("Choose Root Directory");
    gb_root_directory->setMaximumHeight(60);
    gb_root_directory->setAlignment(Qt::AlignTop);
    gb_root_directory->setStyleSheet("QGroupBox { font-weight: bold; } ");

    auto gl_root_directory = new QGridLayout(gb_root_directory);
    gl_root_directory->setAlignment(Qt::AlignTop);

    le_root_directory = new QLineEdit(current_dir->path());
    le_root_directory->setReadOnly(true);
    gl_root_directory->addWidget(le_root_directory, 0, 0, 0, 2);

    QPushButton* pb_choose_dir = new QPushButton("Choose Root Directory");
    QObject::connect(pb_choose_dir, &QPushButton::clicked, this, &MainWindow::chooseRootDirectory);
    gl_root_directory->addWidget(pb_choose_dir, 0, 3, 0, 4);

    QGroupBox* gb_current_directory= new QGroupBox("Current Directory");
    gb_current_directory->setAlignment(Qt::AlignTop);
    gb_current_directory->setStyleSheet("QGroupBox { font-weight: bold; } ");

    auto gl_current_directory = new QGridLayout(gb_current_directory);

    le_current_directory = new QLineEdit(current_dir->path());
    le_current_directory->setReadOnly(true);
    gl_current_directory->addWidget(le_current_directory, 0, 0);

    QPushButton* pb_dir_up = new QPushButton("Directory Up");
    QObject::connect(pb_dir_up, &QPushButton::clicked, this, &MainWindow::directoryUp);
    gl_current_directory->addWidget(pb_dir_up, 0, 1);

    lw_list_directories_in_current = new QListWidget(this);
    QObject::connect(lw_list_directories_in_current, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(itemDoubleClicked(QListWidgetItem*)));
    gl_current_directory->addWidget(lw_list_directories_in_current, 1, 0, 1, 2);

    gl_directory->addWidget(gb_root_directory, 0, 0);
    gl_directory->addWidget(gb_current_directory, 1, 0, 3, 0);
}

void MainWindow::initInfoPathOfWindow(){
    QGroupBox* gb_info= new QGroupBox("Information about current Directory");
    gb_info->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    gb_info->setStyleSheet("QGroupBox { font-weight: bold; } ");

    auto gl_info= new QGridLayout(gb_info);
    gl_info->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    auto number_of_subdirectories_lab = new QLabel("Number of subdirectories");
    number_of_subdirectories_lnedit = new QLineEdit("");
    number_of_subdirectories_lnedit->setReadOnly(true);

    gl_info->addWidget(number_of_subdirectories_lab, 0, 0);
    gl_info->addWidget(number_of_subdirectories_lnedit, 1, 0);

    QGroupBox* gb_stat = new QGroupBox("Statiscis");
    gb_info->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    gb_info->setStyleSheet("QGroupBox { font-weight: bold; } ");
    gb_info->setMaximumWidth(500);

    auto mainWidget = new QWidget(gb_stat);
    gridLayout = new QGridLayout(mainWidget);
    gridLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    mainWidget->setLayout(gridLayout);

    auto scrollArea = new QScrollArea;
    scrollArea->setWidget(mainWidget);
    scrollArea->setWidgetResizable(true);

    auto mainLayout = new QHBoxLayout(gb_stat);
    gb_stat->setLayout(mainLayout);
    mainLayout->addWidget(scrollArea);

    gl_info->addWidget(gb_stat, 2, 0);

    main_grlayout->addWidget(gb_info, 0, 7, 0, 9);
}

void MainWindow::updateInformationAboutDir(const CharacteristicsOfDir& current_statistics){
    number_of_subdirectories_lnedit->setText(QString::number(current_statistics.count_of_directories));
    for (int i = 0; i < gridLayout->count(); i++)
    {
       gridLayout->itemAt(i)->widget()->deleteLater();
    }

    for(const auto& group : current_statistics.list_of_groups){
        gridLayout->addWidget(createGroupBoxStatistic(group));
    }
}

QGroupBox* MainWindow::createGroupBoxStatistic(const Group& current_group){
    auto result = new QGroupBox(current_group.group_type);
    result->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    result->setStyleSheet("QGroupBox { font-weight: bold; } ");
    result->setFixedHeight(170);

    auto gl_result= new QGridLayout(result);
    gl_result->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    auto count_label = new QLabel("Count of files");
    auto count_lnedit = new QLineEdit(QString::number(current_group.characteristics.count_of_files));
    count_lnedit->setReadOnly(true);

    auto total_label = new QLabel("Total size");
    auto total_lnedit = new QLineEdit(QString::number(current_group.characteristics.total_size));
    total_lnedit->setReadOnly(true);

    auto average_label = new QLabel("Average size");
    auto average_lnedit = new QLineEdit(QString::number(current_group.characteristics.average_size));
    average_lnedit->setReadOnly(true);

    gl_result->addWidget(count_label, 0, 0);
    gl_result->addWidget(count_lnedit, 1, 0);

    gl_result->addWidget(total_label, 2, 0);
    gl_result->addWidget(total_lnedit, 3, 0);

    gl_result->addWidget(average_label, 4, 0);
    gl_result->addWidget(average_lnedit, 5, 0);

    return result;
}



