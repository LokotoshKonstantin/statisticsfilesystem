#include "session.h"
#include "rbworker.h"

Session::Session(QObject *parent) : QObject(parent)
{

}

Session::~Session()
{
    stopThreads();
}

void Session::addThread(QDir* current_dir)
{
    qRegisterMetaType<CharacteristicsOfDir>();
    RBWorker* worker = new RBWorker(current_dir);
    QThread* thread = new QThread;
    worker->moveToThread(thread);
    connect(thread, &QThread::started, worker, &RBWorker::process);
    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(this, SIGNAL(stopAll()), worker, SLOT(deleteLater()));
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(worker, &RBWorker::returnCalculatedResult, this, &Session::processingResult);
    thread->start();
    return ;
}

void Session::stopThreads()  /* принудительная остановка всех потоков */
{
    emit  stopAll();
}

void Session::processingResult(const QVariant& data){
    emit returnCalculatedResult(data);
}


