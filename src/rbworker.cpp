#include "rbworker.h"

RBWorker:: RBWorker(QDir* current_dir_)
{
    sb = NULL;
    current_dir = current_dir_;
}

RBWorker::~ RBWorker ()
{
    if (sb != NULL) {
        delete sb;
    }
}

void RBWorker::process()
{
    sb = new StatisticsBuilder(current_dir);
    QObject::connect(sb, &StatisticsBuilder::returnCalculatedResult,
                     this, &RBWorker::processingResult);
    sb->process();
    emit finished();
    return ;
}

void RBWorker::processingResult(const QVariant& data){
    emit returnCalculatedResult(data);
}
