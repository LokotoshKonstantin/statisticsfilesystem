# StatisticsFileSystem

![Снимок_экрана__3_](/uploads/6170e578a59cee4f8591498b4506c258/Снимок_экрана__3_.png)

**Main application window**

****Instruction manual:****
1. Launch  StatisticsFileSystem.exe file;
2. Click "Choose Root Directory" to select the folder that will be the top of the tree;

-The list will show all the subfolders that you can go to. Double-click to go to the selected folder;

-You can return to the previous level if you click "Directory up";

*Warning: you can't go higher than "Root Directory".*

-The "Information about current directory" window shows statistics on current information:
- number of files, total size, and average size for each group of files.
- number of subdirectories.